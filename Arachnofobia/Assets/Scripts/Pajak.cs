﻿using UnityEngine;
using System.Collections;

public class Pajak : MonoBehaviour 
{
    public Transform lewe, prawe;
    private float speedL = 1.0f, speedR = 1.0f, speedA = 1.0f, speed = 0.004f;

	void Start () 
    {
        animation["MoveL"].layer = 1;
        animation["MoveR"].layer = 2;

        animation["MoveAbdomen"].layer = 0;

        animation["MoveL"].AddMixingTransform(lewe);
        animation["MoveR"].AddMixingTransform(prawe);
        
        animation.Play("MoveAbdomen");
        animation.Play("MoveL");
        animation.Play("MoveR");
	}
	
	void Update () 
    {
        transform.position += transform.forward * speed;

        animation["MoveL"].speed = speedL;
        animation["MoveR"].speed = speedR;
        animation["MoveAbdomen"].speed = speedA;

        transform.Rotate(Vector3.up * -0.185f);
	}
}
